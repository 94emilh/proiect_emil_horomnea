## Wantsome - Bank management application

### 1. Description

 - This  a bank management __application__ made as a final project at the end of Java course.
 The app mainly allows an user to manage the users of a bank.
 
Supported actions:
 - add/update/delete users
 - add/update/delete accounts
 - add/update/delete transactions
 - view all the above categories in separate pages and can filter/sort by several specific
 criterias depending on the category
 - show some statistics (and graphs) about the main info regarding each of the above categories

### 2. Setup

No setup needed, just start the application. If the database is missing
(like on first startup), it will create a new database (of type SQLite,
stored in a local file named 'bank_management_application.db'), and use it to save the future data.

### 3. Technical details

__User interface__

The project includes a single type of user interface -> web app (started with Main class)

__Technologies__

- main code is written in Java (minimum version: 8)
- it uses a small embedded database of type SQLite, using SQL and JDBC to
  connect the Java code to it
- it uses Spark micro web framework (which includes an embedded web server, Jetty)
- web pages: using the Velocity templating engine, to separate the UI code 
  from Java code; UI code consists of basic HTML and CSS code (and a little Javascript)
- charts: using JavaScript library Google Charts (https://developers.google.com/chart)
- web services interface: uses REST principles to define the API, and JSON to
  encode requests/responses (using Gson library)
  
- includes some unit tests for DB part (using JUnit library) __to add in Git after finishing JUnit tests!!!__

__Code structure__

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init
    and connect to the db
  - ui - code related to the interface/presentation layer
  - root package - the main classes for the 3 types of interfaces it supports

- web resources are found in `main/resources` folder:
  - under `/public` folder - static resources to be served by the web server
    directly (images, css files)
  - all other (directly under `/resources`) - the Velocity templates
  
Note: the focus of this project is on the back-end part, not so much on the 
front-end part.
