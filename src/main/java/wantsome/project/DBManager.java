package wantsome.project;

import org.sqlite.SQLiteConfig;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {

    private static String dbFile = "bank_management_application.db";

    public static void setDbFile(String newDbFile) {
        dbFile = newDbFile;
        System.out.println("Using custom SQLite db file: " + new File(dbFile).getAbsolutePath());
    }

    public static Connection getConnection() throws SQLException {

        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true); //enable FK support (disabled by default)
        config.setDateStringFormat("yyyy-MM-dd HH:mm:ss"); //this also seems important, to avoid some problems with date/time fields..

        return DriverManager.getConnection("jdbc:sqlite:" + dbFile, config.toProperties());
    }
}

