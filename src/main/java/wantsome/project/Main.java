package wantsome.project;

import spark.Spark;
import wantsome.project.db.service.DbInitService;
import wantsome.project.web.*;


import static spark.Spark.*;

/**
 * Main class of the application. Using Spark framework.
 */
public class Main {

    public static void main(String[] args) {
        DbInitService.createMissingTables();

        startWebServer();
    }

    private static void startWebServer() {

        Spark.port(8080);
        staticFileLocation("public");

        redirect.get("/","/homepage");

        //homepage
        get("/homepage", HomePageController::showHomepage);

        //customers page
        get("/customers", CustomerPageController::showCustomerPage);

        get("/customers/add", AddEditCustomerPageController::showAddForm);
        post("/customers/add", AddEditCustomerPageController::handleAddUpdatePostRequest);

        get("/customers/update/:id", AddEditCustomerPageController::showUpdateForm);
        post("/customers/update/:id", AddEditCustomerPageController::handleAddUpdatePostRequest);

        get("/customers/delete/:id", CustomerPageController :: handleDeleteRequest);

        //accounts page
        get("/accounts", AccountPageController::showAccountPage);

        get("/accounts/add", AddEditAccountPageController::showAddForm);
        post("/accounts/add", AddEditAccountPageController::handleAddUpdatePostRequest);

        get("/accounts/update/:id", AddEditAccountPageController::showUpdateForm);
        post("/accounts/update/:id", AddEditAccountPageController::handleAddUpdatePostRequest);

        get("/accounts/delete/:id", AccountPageController :: handleDeleteRequest);

        //transactions page
        get("/transactions", TransactionPageController::showTransactionPage);

        get("/transactions/add", AddEditTransactionPageController::showAddForm);
        post("/transactions/add", AddEditTransactionPageController::handleAddUpdatePostRequest);

        get("/transactions/update/:id", AddEditTransactionPageController::showUpdateForm);
        post("/transactions/update/:id", AddEditTransactionPageController::handleAddUpdatePostRequest);

        get("/transactions/delete/:id", TransactionPageController :: handleDeleteRequest);

        //error handling

        exception(Exception.class, ErrorPageController::handleException);

        awaitInitialization();
        System.out.println("\nServer started: http://localhost:8080");
    }

}
