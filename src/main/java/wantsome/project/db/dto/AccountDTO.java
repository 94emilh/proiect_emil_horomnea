package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class AccountDTO {

    private final long id;
    private final String name;
    private final Double balance;
    private final Date dateOpened;
    private final long customerId;

    public AccountDTO(long id, String name, Double balance, Date dateOpened, long customerId) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.dateOpened = dateOpened;
        this.customerId = customerId;
    }

    public AccountDTO(String name, double balance, Date dateOpened, long customerId){
        this(-1,name,balance,dateOpened,customerId);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getBalance() {
        return balance;
    }

    public Date getDateOpened() {
        return dateOpened;
    }

    public long getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", dateOfOpening=" + dateOpened +
                ", customerId=" + customerId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return Double.compare(that.balance, balance) == 0 &&
                customerId == that.customerId &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(dateOpened, that.dateOpened);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, balance, dateOpened, customerId);
    }
}
