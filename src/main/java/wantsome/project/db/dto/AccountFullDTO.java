package wantsome.project.db.dto;

import java.sql.Date;

public class AccountFullDTO {

    private final int id;
    private final String name;
    private final Double balance;
    private final Date dateOpened;
    private final long customerId;
    private final Role customerRole;

    public AccountFullDTO(int id, String name, Double balance, Date dateOpened, long customerId, Role customerRole) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.dateOpened = dateOpened;
        this.customerId = customerId;
        this.customerRole = customerRole;
    }

    public AccountFullDTO(String name, double balance, Date dateOpened, long customerId, Role customerRole){
        this(-1,name,balance,dateOpened,customerId,customerRole);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getBalance() {
        return balance;
    }

    public Date getDateOpened() {
        return dateOpened;
    }

    public long getCustomerId() {
        return customerId;
    }

    public Role getCustomerRole() {
        return customerRole;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", dateOpened=" + dateOpened +
                ", customerId=" + customerId +
                ", customerRole=" + customerRole +
                '}';
    }


}
