package wantsome.project.db.dto;

public class CustomerDTO {

    private final long id;
    private final String name;
    private final Role role;
    private final String username;
    private final String password;
    private final long phoneNumber;
    private final long ssn;

    public CustomerDTO(long id, String name, Role role, String username, String password, long phoneNumber, long ssn) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.ssn = ssn;
    }

    public CustomerDTO(String name, Role role, String username, String password, long phoneNumber, long ssn){
        this(-1,name,role,username,password,phoneNumber,ssn);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public long getSsn() {
        return ssn;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role=" + role +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", ssn=" + ssn +
                '}';
    }
}
