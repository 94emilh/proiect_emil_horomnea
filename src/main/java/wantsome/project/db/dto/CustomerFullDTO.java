package wantsome.project.db.dto;

public class CustomerFullDTO {

    private final int id;
    private final String name;
    private final int accountId;
    private final double balance;
    private final Role role;
    private final String username;
    private final String password;
    private final long phoneNumber;
    private final long ssn;

    public CustomerFullDTO(int id, String name, int accountId, double balance, Role role, String username, String password, long phoneNumber, long ssn) {
        this.id = id;
        this.name = name;
        this.accountId = accountId;
        this.balance = balance;
        this.role = role;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.ssn = ssn;
    }

    public CustomerFullDTO(String name, int accountId, double balance, Role role, String username, String password, long phoneNumber, long ssn){
        this(-1,name, accountId, balance, role,username,password,phoneNumber,ssn);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAccountId() {
        return accountId;
    }

    public double getBalance() {
        return balance;
    }

    public Role getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public long getSsn() {
        return ssn;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accountId=" + accountId +
                ", balance=" + balance +
                ", role=" + role +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", ssn=" + ssn +
                '}';
    }
}
