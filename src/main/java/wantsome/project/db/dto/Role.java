package wantsome.project.db.dto;

public enum Role {
    ADMIN("Admin"),
    CUSTOMER("Customer");

    private final String label;

    Role(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
