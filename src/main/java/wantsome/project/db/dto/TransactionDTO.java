package wantsome.project.db.dto;

import wantsome.project.db.service.TransactionDAO;

import java.sql.Date;

public class TransactionDTO {

    private final long id;
    private final long accountId;
    private final double amount;
    private final Date date;
    private final String details;
    private final Long destinationAcc; //will be null for type withdraw/deposit
    private final TransactionType type;

    public TransactionDTO(long id, long accountId, double amount, Date date, String details, Long destinationAcc, TransactionType type) {
        this.id = id;
        this.accountId = accountId;
        this.amount = amount;
        this.date = date;
        this.details = details;
        this.destinationAcc = destinationAcc;
        this.type = type;
    }

    public TransactionDTO(long accountId, double amount, Date date, String details, Long destinationAcc, TransactionType type) {
        this(-1,accountId,amount,date,details,destinationAcc,type);
    }

    public long getId() {
        return id;
    }

    public long getAccountId() {
        return accountId;
    }

    public double getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public String getDetails() {
        return details;
    }

    public Long getDestinationAcc() {
        return destinationAcc;
    }

    public TransactionType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "id=" + id +
                ", accountId='" + accountId + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", details='" + details + '\'' +
                ", destinationAcc=" + destinationAcc +
                ", type=" + type +
                '}';
    }
}
