package wantsome.project.db.dto;

public class UserDTO {

    private final long id;
    private final String name; //for admins
    private final long customerId;
    private final Role role;
    private final String username;
    private final String password;

    public UserDTO(long id, String name, long customerId, Role role, String username, String password) {
        this.id = id;
        this.name = name;
        this.customerId = customerId;
        this.role = role;
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getCustomerId() {
        return customerId;
    }

    public Role getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "UsersDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", customerId=" + customerId +
                ", role=" + role +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
