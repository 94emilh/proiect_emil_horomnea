package wantsome.project.db.service;

import wantsome.project.DBManager;
import wantsome.project.db.dto.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountDAO {

    public static List<AccountDTO> getAll() {
//        String sql = "select A.*, c.Role as Customer_Role from ACCOUNT A\n" +
//                "join CUSTOMER c on c.ID=a.CUSTOMER_ID";

        String sql = "SELECT * FROM ACCOUNT";


        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<AccountDTO> listOfAccounts = new ArrayList<>();

            while (rs.next()) {
                listOfAccounts.add(
                        new AccountDTO(
                                rs.getLong("ID"),
                                rs.getString("NAME"),
                                rs.getDouble("BALANCE"),
                                rs.getDate("DATE_OPENED"),
                                rs.getLong("CUSTOMER_ID")));
            }
            return listOfAccounts;

        } catch (SQLException s) {
            throw new RuntimeException("Error loading all accounts: " + s.getMessage());
        }
    }

    public static Optional<AccountDTO> get(long id) {

        String sql = "SELECT * FROM ACCOUNT WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new AccountDTO(
                            rs.getLong("ID"),
                            rs.getString("NAME"),
                            rs.getDouble("BALANCE"),
                            rs.getDate("DATE_OPENED"),
                            rs.getLong("CUSTOMER_ID")));
                }
            }

            return Optional.empty();

        } catch (SQLException s) {
            throw new RuntimeException("Error loading account with ID " + id + " : " + s.getMessage());
        }
    }

    public static void insert(AccountDTO account) {
        String sql = "INSERT INTO Account (NAME, BALANCE, DATE_OPENED, CUSTOMER_ID)" +
                "VALUES (?,?,?,?)";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, account.getName());
            ps.setDouble(2, account.getBalance());
            ps.setDate(3, account.getDateOpened());
            ps.setLong(4, account.getCustomerId());

            ps.execute();

        } catch (SQLException s) {
            String details = s.getMessage().contains("UNIQUE constraint failed: ACCOUNT.NAME") ||
                    s.getMessage().contains("Abort due to constraint violation (FOREIGN KEY constraint failed)") ?
                    "An account with same name already exists or there is no existing customer with ID " + account.getCustomerId() : s.getMessage();
            throw new RuntimeException(details);
        }
    }

    public static void updateBalance(Optional<AccountDTO> account, double balanceUpdate) {
        String sql = "UPDATE ACCOUNT " +
                "SET BALANCE = ? " +
                "WHERE ID = ?";


        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (account.isPresent()) {

                AccountDTO accountDTO = account.get();
                if(accountDTO.getBalance()<-balanceUpdate && balanceUpdate<0){
                    throw new RuntimeException("Could not update balance, withdraw amount (" + -balanceUpdate + ") is bigger than " +
                            "account balance (" + accountDTO.getBalance() + ")!");
                }else{
                    ps.setDouble(1, accountDTO.getBalance() + balanceUpdate);
                }
                ps.setLong(2, accountDTO.getId());

                ps.executeUpdate();
            } else {
                System.out.println("Account with the given ID does not exists!");
            }

        } catch (SQLException s) {
            throw new RuntimeException("Error while updating balance for account " + account + " : " + s.getMessage());
        }

    }

    public static void update(AccountDTO account){
        String sql = "UPDATE ACCOUNT " +
                "SET NAME = ?, " +
                "BALANCE = ?," +
                "DATE_OPENED = ?, " +
                "CUSTOMER_ID = ? " +
                "WHERE ID = ?";
        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)){

            ps.setString(1,account.getName());
            ps.setDouble(2,account.getBalance());
            ps.setDate(3,account.getDateOpened());
            ps.setLong(4,account.getCustomerId());
            ps.setLong(5,account.getId());

            ps.executeUpdate();

        } catch (SQLException s) {
            String details = s.getMessage().contains("UNIQUE constraint failed: ACCOUNT.NAME") ?
                    "An account with same username already exists" : s.getMessage();
            throw new RuntimeException("Error updating customer " + account.getId() + ": " + details);
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM ACCOUNT WHERE ID=?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting account " + id + ": " + e.getMessage());
        }
    }

    public static void deleteTable() throws SQLException {
        String sql = "DROP TABLE ACCOUNT";
        try (Connection conn = DBManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        }
    }

    }
