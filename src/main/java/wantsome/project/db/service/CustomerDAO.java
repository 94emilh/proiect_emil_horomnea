package wantsome.project.db.service;

import jdk.jshell.spi.ExecutionControl;
import wantsome.project.DBManager;
import wantsome.project.db.dto.CustomerDTO;
import wantsome.project.db.dto.CustomerFullDTO;
import wantsome.project.db.dto.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerDAO {

    public static List<CustomerDTO> getAll() {
//        String sql = "select C.*, a.ID as Account_ID,a.BALANCE as Balance from CUSTOMER c\n" +
//                "join ACCOUNT a on a.CUSTOMER_ID=c.ID";

        String sql = "SELECT * from CUSTOMER " +
                "ORDER BY ID";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<CustomerDTO> listOfCustomers = new ArrayList<>();

            while (rs.next()) {
                listOfCustomers.add(new CustomerDTO(
                        rs.getInt("ID"),
                        rs.getString("NAME"),
                        Role.valueOf(rs.getString("ROLE")),
                        rs.getString("USERNAME"),
                        rs.getString("PASSWORD"),
                        rs.getLong("PHONE_NUMBER"),
                        rs.getLong("SSN")));
            }

            return listOfCustomers;

        } catch (SQLException s) {
            throw new RuntimeException("Error loading all customers: " + s.getMessage());
        }
    }

    public static Optional<CustomerDTO> get(long id) {

        String sql = "SELECT * FROM CUSTOMER WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(
                            new CustomerDTO(
                            rs.getLong("ID"),
                            rs.getString("NAME"),
                            Role.valueOf(rs.getString("ROLE")),
                            rs.getString("USERNAME"),
                            rs.getString("PASSWORD"),
                            rs.getLong("PHONE_NUMBER"),
                            rs.getLong("SSN")));
                }
                return Optional.empty(); //if not found
            }


        } catch (SQLException s) {
            throw new RuntimeException("Error loading customer: " + s.getMessage());
        }

    }

    public static void insert(CustomerDTO customer){
        String sql = "INSERT INTO CUSTOMER (NAME, ROLE, USERNAME, PASSWORD, PHONE_NUMBER, SSN)" +
                "VALUES (?,?,?,?,?,?)";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1,customer.getName());
            ps.setString(2,customer.getRole().name());
            ps.setString(3,customer.getUsername());
            ps.setString(4,customer.getPassword());
            ps.setLong(5,customer.getPhoneNumber());
            ps.setLong(6,customer.getSsn());

            ps.execute();

        } catch (SQLException s) {
            String details = s.getMessage().contains("UNIQUE constraint failed: CUSTOMER.USERNAME") ?
                    "a customer with same username already exists" : s.getMessage();
            throw new RuntimeException("Error saving new customer: " + details);
        }
    }

    public static void update(CustomerDTO customer){
        String sql = "UPDATE CUSTOMER " +
                "SET NAME = ?, " +
                "ROLE = ?," +
                "USERNAME = ?, " +
                "PASSWORD = ?, " +
                "PHONE_NUMBER = ?," +
                "SSN = ? " +
                "WHERE ID = ?";
        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)){

            ps.setString(1,customer.getName());
            ps.setString(2,customer.getRole().name());
            ps.setString(3,customer.getUsername());
            ps.setString(4,customer.getPassword());
            ps.setLong(5,customer.getPhoneNumber());
            ps.setLong(6,customer.getSsn());
            ps.setLong(7,customer.getId());

            ps.executeUpdate();

        } catch (SQLException s) {
            String details = s.getMessage().contains("UNIQUE constraint failed: CUSTOMER.USERNAME") ?
                    "a customer with same username already exists" : s.getMessage();
            throw new RuntimeException("Error updating customer " + customer.getId() + ": " + details);
        }
    }

    public static void delete(long id){

        String sql = "DELETE FROM CUSTOMER WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1,id);

            ps.execute();

        } catch (SQLException s) {
            throw new RuntimeException("Error while deleting note " + id + " : " + s.getMessage());
        }
    }

    public static void deleteTable() throws SQLException {
        String sql = "DROP TABLE CUSTOMER";
        try (Connection conn = DBManager.getConnection();
        Statement st = conn.createStatement()){
            st.execute(sql);
        }
    }

}
