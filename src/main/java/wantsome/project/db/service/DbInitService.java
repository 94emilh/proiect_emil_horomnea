package wantsome.project.db.service;



import wantsome.project.DBManager;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.TransactionType;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DbInitService {

    public static void createMissingTables() {

        String sqlForCreatingCustomer = "CREATE TABLE IF NOT EXISTS CUSTOMER(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NAME TEXT UNIQUE NOT NULL, " +
                "ROLE TEXT CHECK(ROLE IN ('" + Role.CUSTOMER + "', '" + Role.ADMIN + "')) NOT NULL, " +
                "USERNAME TEXT UNIQUE NOT NULL, " +
                "PASSWORD TEXT NOT NULL, " +
                "PHONE_NUMBER INTEGER," +
                "SSN INTEGER UNIQUE NOT NULL)";


        String sqlForCreatingAccount = "CREATE TABLE IF NOT EXISTS ACCOUNT(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NAME TEXT UNIQUE NOT NULL, " +
                "BALANCE REAL NOT NULL, " +
                "DATE_OPENED DATETIME NOT NULL, " +
                "CUSTOMER_ID INTEGER REFERENCES CUSTOMER(ID) ON DELETE CASCADE)";

        String sqlForCreatingTransaction = "CREATE TABLE IF NOT EXISTS TRANSACTIONS(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "ACCOUNT_ID INTEGER NOT NULL REFERENCES ACCOUNT(ID) ON DELETE CASCADE, " +
                "AMOUNT REAL NOT NULL, " +
                "DATE_OF_TR DATETIME NOT NULL," +
                "DETAILS TEXT, " +
                "DESTINATION_ACC INTEGER REFERENCES ACCOUNT(ID) ON DELETE CASCADE, " +
                "TYPE TEXT CHECK(TYPE IN ('" + TransactionType.DEPOSIT + "', '" + TransactionType.WITHDRAW +
                "', '" + TransactionType.TRANSFER + "')) NOT NULL)";



        try (Connection conn = DBManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(sqlForCreatingCustomer);
            st.execute(sqlForCreatingAccount);
            st.execute(sqlForCreatingTransaction);

        } catch (SQLException e) {
            System.err.println("Error creating missing tables: " + e.getMessage());
        }

    }
}
