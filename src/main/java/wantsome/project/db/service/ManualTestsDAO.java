package wantsome.project.db.service;

import wantsome.project.DBManager;
import wantsome.project.db.dto.*;

import java.sql.Date;
import java.sql.SQLException;

public class ManualTestsDAO {

    public static void main(String[] args) throws SQLException {

        DbInitService.createMissingTables();


        //CustomerDAO.insert(new CustomerDTO("Emil", Role.CUSTOMER,"94emilh","parola",7555124,1940142112));
        //CustomerDAO.insert(new CustomerDTO("Marta",Role.CUSTOMER,"95marta","parola1",7212315,192521230));

//        System.out.println("List of customers: ");
//        CustomerDAO.getAll()
//                .forEach(System.out::println);


//        AccountDAO.insert(new AccountDTO("Checking Acc 001", 1000, Date.valueOf("2020-01-01"), 1));
        //AccountDAO.insert(new AccountDTO("Checking Acc 003", 5000, Date.valueOf("2020-01-02"), 4));
//
//        System.out.println("\nList of accounts: ");
//        AccountDAO.getAll()
//                .forEach(System.out::println);
//
        //TransactionDAO.insert(new TransactionDTO(10,500,Date.valueOf("2020-01-02"),null,null,TransactionType.DEPOSIT));
        //TransactionDAO.insert(new TransactionDTO(2,1000,Date.valueOf("2020-01-30"),"N/A",null,TransactionType.DEPOSIT));
        //TransactionDAO.insert(new TransactionDTO(2, 100, Date.valueOf("2020-02-30"), "N/A", null, TransactionType.WITHDRAW));

        System.out.println("\nList of transactions: ");
        TransactionDAO.getAll()
                .forEach(System.out::println);

        TransactionDTO newTransaction = new TransactionDTO(1,1,110,Date.valueOf("2020-10-20"),null,2L,TransactionType.TRANSFER);
        TransactionDAO.update(newTransaction);

        System.out.println("\nList of transactions after updating transaction 1: ");
        TransactionDAO.getAll()
                .forEach(System.out::println);
//
//        System.out.println("\nList of accounts after transactions: ");
//        AccountDAO.getAll()
//                .forEach(System.out::println);

        //CustomerDAO.deleteTable();
        //AccountDAO.deleteTable();
        //TransactionDAO.deleteTable();

    }
}
