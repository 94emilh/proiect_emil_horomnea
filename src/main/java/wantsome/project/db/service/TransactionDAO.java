package wantsome.project.db.service;

import wantsome.project.DBManager;
import wantsome.project.db.dto.TransactionDTO;
import wantsome.project.db.dto.TransactionType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransactionDAO {

    public static List<TransactionDTO> getAll() {
        String sql = "SELECT * " +
                "FROM TRANSACTIONS " +
                "ORDER BY ID";

        List<TransactionDTO> listOfTransactions = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                listOfTransactions.add(extractTransactionFromDb(rs));
            }

        } catch (SQLException s) {
            throw new RuntimeException("Error loading all transactions: " + s.getMessage());
        }

        return listOfTransactions;
    }

    private static TransactionDTO extractTransactionFromDb(ResultSet rs) throws SQLException {

        long id = rs.getLong("ID");
        long accountId = rs.getLong("ACCOUNT_ID");
        double amount = rs.getDouble("AMOUNT");
        Date date = rs.getDate("DATE_OF_TR");
        String details = rs.getString("DETAILS");
        Long destinationAcc = rs.getLong("DESTINATION_ACC");
        if (destinationAcc <= 0) {
            destinationAcc = null;
        }
        TransactionType type = TransactionType.valueOf(rs.getString("TYPE"));

        return new TransactionDTO(id, accountId, amount, date, details, destinationAcc, type);
    }

    public static List<TransactionDTO> getAllForACertainAcc(long accountId) {

        String sql = "SELECT *" +
                "FROM TRANSACTIONS" +
                "WHERE ACCOUNT_ID = " + accountId +
                "ORDER BY ID";

        List<TransactionDTO> listOfTransactions = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                listOfTransactions.add(extractTransactionFromDb(rs));
            }

        } catch (SQLException s) {
            throw new RuntimeException("Error loading all transactions for account ID: " + accountId + " : " + s.getMessage());
        }

        return listOfTransactions;
    }

    public static Optional<TransactionDTO> get(long id) {

        String sql = "SELECT * FROM TRANSACTIONS WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    TransactionDTO customer = extractTransactionFromDb(rs);
                    return Optional.of(customer);
                }
            }


        } catch (SQLException s) {
            throw new RuntimeException("Error loading transaction with ID " + id + " : " + s.getMessage());
        }

        return Optional.empty();

    }

    public static void insert(TransactionDTO transaction) {
        String sql = "INSERT INTO TRANSACTIONS (ACCOUNT_ID, AMOUNT, DATE_OF_TR, DETAILS, DESTINATION_ACC, TYPE)" +
                "VALUES (?,?,?,?,?,?)";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, transaction.getAccountId());
            ps.setDouble(2, transaction.getAmount());
            ps.setDate(3, transaction.getDate());
            if (transaction.getDetails() != null) {
                ps.setString(4, transaction.getDetails());
            } else {
                ps.setNull(4, Types.VARCHAR);
            }
            if (transaction.getDestinationAcc() != null) {
                ps.setLong(5, transaction.getDestinationAcc());
            } else {
                ps.setNull(5, Types.INTEGER);
            }
            ps.setString(6, transaction.getType().name());

            ps.execute();

        } catch (SQLException s) {
            String details = s.getMessage().contains("Abort due to constraint violation (FOREIGN KEY constraint failed)") ?
                    "Account ID " + transaction.getAccountId() + " or Destination account " + transaction.getDestinationAcc() +
                            " does not exists, please insert an existing account in each of these fields!" :
                    s.getMessage();
            throw new RuntimeException(details);
        }
        if (transaction.getType().name().equals("DEPOSIT")) {
            if(transaction.getDestinationAcc()!=null){
                throw new RuntimeException("Destination account must be empty for transaction type DEPOSIT!");
            }
            AccountDAO.updateBalance(AccountDAO.get(transaction.getAccountId()), transaction.getAmount());
        }

        if (transaction.getType().name().equals("WITHDRAW")) {
            if(transaction.getDestinationAcc()!=null){
                throw new RuntimeException("Destination account must be empty for transaction type WITHDRAW!");
            }
            AccountDAO.updateBalance(AccountDAO.get(transaction.getAccountId()), -transaction.getAmount());
        }

        if (transaction.getType().name().equals("TRANSFER")) {
            AccountDAO.updateBalance(AccountDAO.get(transaction.getAccountId()), -transaction.getAmount());
            AccountDAO.updateBalance(AccountDAO.get(transaction.getDestinationAcc()), transaction.getAmount());
        }

    }

    public static void update(TransactionDTO transaction) {
        String sql = "UPDATE TRANSACTIONS " +
                "SET ACCOUNT_ID = ?," +
                "AMOUNT = ?," +
                "DATE_OF_TR = ?," +
                "DETAILS = ?," +
                "DESTINATION_ACC = ?," +
                "TYPE = ?" +
                "WHERE ID = ?";
        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, transaction.getAccountId());
            ps.setDouble(2, transaction.getAmount());
            ps.setDate(3, transaction.getDate());
            ps.setString(4, transaction.getDetails());
            ps.setLong(5, transaction.getDestinationAcc());
            ps.setString(6, transaction.getType().name());
            ps.setLong(7,transaction.getId());

            ps.executeUpdate();

        } catch (SQLException s) {
            throw new RuntimeException("Error updating account " + transaction.getId() + ": " + s.getMessage());
        }

    }

    public static void delete(long id) {

        String sql = "DELETE FROM TRANSACTIONS WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException s) {
            System.err.println("Error while deleting transaction " + id + " : " + s.getMessage());
        }
    }

    public static void deleteTable() throws SQLException {
        String sql = "DROP TABLE TRANSACTIONS";
        try (Connection conn = DBManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        }
    }

}
