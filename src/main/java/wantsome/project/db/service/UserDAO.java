package wantsome.project.db.service;

import wantsome.project.DBManager;
import wantsome.project.db.dto.CustomerDTO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.UserDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO {

    public static List<UserDTO> getall(){
        String sql = "SELECT *" +
                "FROM USER" +
                "ORDER BY ID";

        List<UserDTO> listOfUsers = new ArrayList<>();


        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                listOfUsers.add(extractUserFromDb(rs));
            }

        } catch (SQLException s) {
            System.err.println("Error loading all notes: " + s.getMessage());
        }

        return listOfUsers;

    }

    private static UserDTO extractUserFromDb(ResultSet rs) throws SQLException {

        long id = rs.getLong("ID");
        String name = rs.getString("NAME");
        long customerId = rs.getLong("CUSTOMER_ID");
        Role role = Role.valueOf(rs.getString("ROLE"));
        String username = rs.getString("USERNAME");
        String password = rs.getString("PASSWORD");

        return new UserDTO(id,name,customerId,role,username,password);

    }

    private static Optional<UserDTO> get(long id) {

        String sql = "SELECT * FROM NOTES WHERE ID = ?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    UserDTO user = extractUserFromDb(rs);
                    return Optional.of(user);
                }
            }


        } catch (SQLException s) {
            System.err.println("Error loading customer with ID " + id + " : " + s.getMessage());
        }

        return Optional.empty();

    }

    public static void insert(UserDTO user){
        String sql = "INSERT INTO USER (ID, NAME, CUSTOMER_ID, ROLE, USERNAME, PASSWORD)" +
                "VALUES (?,?,?,?,?,?)";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, user.getId());
            ps.setString(2,user.getName());
            ps.setLong(3,user.getCustomerId());
            ps.setString(4,user.getRole().name());
            ps.setString(5,user.getUsername());
            ps.setString(6,user.getPassword());

            ps.execute();

        } catch (SQLException s) {
            System.err.println("Error inserting in db user " + user + " : " + s.getMessage());
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM USER WHERE ID=?";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setLong(1, id);

            //execute it (no results to read after this one)
            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting user " + id + ": " + e.getMessage());
        }
    }



}
