package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.AccountDTO;
import wantsome.project.db.service.AccountDAO;

import java.util.*;

import static java.util.stream.Collectors.*;
import static wantsome.project.web.SparkUtil.render;

public class AccountPageController {

    public enum Order{
        ROLE, NAME, BALANCE, DATE_OPENED, ID, TYPE, DATE, AMOUNT
    }

    public static String showAccountPage(Request request, Response response) {
        List<AccountDTO> listOfAccounts = AccountDAO.getAll();

        //customers to display when sorting by balance/date opened
        Order order = getOrderFromQueryOrSes(request);

        //data for graphs
        List<AccountDTO> accountsToDisplay = getAccountsToDisplay(listOfAccounts,order);
        long accountsVsBalanceCount1 = computeAccountAndBalance(listOfAccounts,1,500);
        long accountsVsBalanceCount2 = computeAccountAndBalance(listOfAccounts,500,1500);
        long accountsVsBalanceCount3 = computeAccountAndBalance(listOfAccounts,1500,2500);
        long accountsVsBalanceCount4 = computeAccountAndBalance(listOfAccounts,2500,Double.MAX_VALUE);

        Map<String,Object> model = new HashMap<>();

        model.put("accounts",accountsToDisplay);
        model.put("crtOrder",order);
        model.put("firstCategoryCount", accountsVsBalanceCount1);
        model.put("secondCategoryCount", accountsVsBalanceCount2);
        model.put("thirdCategoryCount", accountsVsBalanceCount3);
        model.put("fourthCategoryCount", accountsVsBalanceCount4);

        return render("account.vm",model);
    }

    public static long computeAccountAndBalance(List<AccountDTO> listOfAccounts, double minValue, double maxValue){
        return listOfAccounts.stream()
                .filter(s -> s.getBalance() >= minValue && s.getBalance() <maxValue)
                .count();
    }

    private static Order getOrderFromQueryOrSes(Request response) {
        String param = fromQueryOrSession(response, "order");
        return param == null ?
                Order.DATE_OPENED : //default
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name); //read it from current request (if sent)
        if (param != null) {
            request.session().attribute(name, param); //if found, save it to session for later
        } else {
            param = request.session().attribute(name); //if not found, try to read it from session
        }
        return param;
    }

    private static List<AccountDTO> getAccountsToDisplay(List<AccountDTO> allAccounts, Order order) {
        return allAccounts.stream()
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<AccountDTO> getDisplayComparator(Order order) {
        if (order == Order.DATE_OPENED) {
            Date defaultDate = new Date(0); //start of time, to force them to appear first in list
            return (i1, i2) -> {
                Date d1 = i1.getDateOpened() != null ? i1.getDateOpened() : defaultDate;
                Date d2 = i2.getDateOpened() != null ? i2.getDateOpened() : defaultDate;
                return d1.compareTo(d2);
            };
        }else if (order == Order.ID){
            return Comparator.comparing(AccountDTO::getId);
        }
        return Comparator.comparing(AccountDTO::getBalance).reversed();
    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String id = request.params("id");
        AccountDAO.delete(Long.parseLong(id));
        response.redirect("/accounts");
        return response;
    }

}
