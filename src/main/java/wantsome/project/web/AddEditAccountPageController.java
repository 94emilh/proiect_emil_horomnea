package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.AccountDTO;
import wantsome.project.db.service.AccountDAO;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditAccountPageController {

    public static String showAddForm(Request request, Response response) {
        //show page with empty form (with some defaults)
        return renderAddUpdateForm("", "", "", "","", null);
    }

    public static String showUpdateForm(Request request, Response response){

        String id = request.params("id");
        Optional<AccountDTO> optAccount = AccountDAO.get(Integer.parseInt(id));
        if (optAccount.isEmpty()){
            throw new RuntimeException("Account " + id + " not found!");
        }

        AccountDTO account = optAccount.get();
        return renderAddUpdateForm(
                String.valueOf(account.getId()),
                account.getName(),
                String.valueOf(account.getBalance()),
                String.valueOf(account.getDateOpened()),
                String.valueOf(account.getCustomerId()),
                "");
    }

    private static String renderAddUpdateForm(String id, String name, String balance, String dateOpened,
                                              String customerId, String errorMsg){
        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate",id!=null && !id.isEmpty());

        model.put("prevId",id);
        model.put("prevName",name);
        model.put("prevBalance", balance);
        model.put("prevDateOpened", dateOpened);
        model.put("prevCustomerId",customerId);
        model.put("errorMsg",errorMsg);

        return render("add_edit_account.vm",model);
    }

    public static Object handleAddUpdatePostRequest(Request request,Response response){

        String id = request.queryParams("id");
        String name = request.queryParams("name");
        String balance = request.queryParams("balance");
        String dateOpened = request.queryParams("dateOpened");
        String customerId = request.queryParams("customerId");

        String serverAction = request.queryParams("serverAction");
        if (serverAction.equals("refresh")) {
            return renderAddUpdateForm(id, name, balance, dateOpened, customerId, null);
        }

        return tryPerformAddUpdateAction(response,id, name, balance, dateOpened, customerId);
    }

    private static Object tryPerformAddUpdateAction (Response response, String id, String name, String balance, String dateOpened, String customerId){
        try {
            AccountDTO account = validateAndBuildAccount(id,name,balance,dateOpened,customerId);

            if(account.getId() > 0){
                AccountDAO.update(account);
            }else {
                AccountDAO.insert(account);
            }

            response.redirect("/accounts");
            return response;
        } catch (Exception e){
            return renderAddUpdateForm(id,name,balance,dateOpened,customerId,e.getMessage());
        }

    }

    private static AccountDTO validateAndBuildAccount(String id, String name, String balance, String dateOpened, String customerId){

        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;

        if (name == null || name.isEmpty()) {
            throw new RuntimeException("Name is required!");
        }

        Double balanceValue = Double.parseDouble(balance);

        Date dateOpenedValue = null;
        if(dateOpened!=null && !dateOpened.isEmpty()){
            try {
                dateOpenedValue = Date.valueOf(dateOpened);
            } catch (Exception e) {
                throw new RuntimeException("Invalid due date value: '" + dateOpened +
                        "', must be a date in format: yyyy-[m]m-[d]d");
            }
        }

        long customerIdValue = Long.parseLong(customerId);


        return new AccountDTO(idValue, name, balanceValue, dateOpenedValue, customerIdValue);

    }


}
