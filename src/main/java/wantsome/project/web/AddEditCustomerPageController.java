package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.CustomerDTO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.service.CustomerDAO;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditCustomerPageController {

    public static String showAddForm(Request request,Response response){
        return renderAddUpdateForm("","",Role.CUSTOMER.name(),"","","","",null);
    }


    public static String showUpdateForm(Request request, Response response){

        String id = request.params("id");
        Optional<CustomerDTO> optCustomer = CustomerDAO.get(Integer.parseInt(id));
        if (optCustomer.isEmpty()){
            throw new RuntimeException("Customer " + id + " not found!");
        }

        CustomerDTO customer = optCustomer.get();
        return renderAddUpdateForm(
                String.valueOf(customer.getId()),
                customer.getName(),
                customer.getRole().name(),
                customer.getUsername(),
                customer.getPassword(),
                String.valueOf(customer.getPhoneNumber()),
                String.valueOf(customer.getSsn()),
                "");
    }


    private static String renderAddUpdateForm(String id, String name, String role, String username,
                                              String password, String phoneNumber,
                                              String ssn, String errorMsg){
        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate",id!=null && !id.isEmpty());

        model.put("prevId",id);
        model.put("prevName",name);
        model.put("prevRole", role);
        model.put("prevUsername", username);
        model.put("prevPassword",password);
        model.put("prevPhoneNumber",phoneNumber);
        model.put("prevSsn",ssn);
        model.put("errorMsg",errorMsg);


        model.put("roleOptions", Role.values());

        return render("add_edit_customer.vm",model);
    }

    public static Object handleAddUpdatePostRequest(Request request,Response response){

        String id = request.queryParams("id");
        String name = request.queryParams("name");
        String role = request.queryParams("role");
        String username = request.queryParams("username");
        String password = request.queryParams("password");
        String phoneNumber = request.queryParams("phoneNumber");
        String ssn = request.queryParams("ssn");


        return tryPerformAddUpdateAction(response,id,name,role,username,password,phoneNumber,ssn);
    }

    private static Object tryPerformAddUpdateAction (Response response, String id, String name, String role, String username, String password,
                                                     String phoneNumber, String ssn){
        try {
            CustomerDTO customer = validateAndBuildCustomer(id,name,role,username,password,phoneNumber,ssn);

            if(customer.getId() > 0){
                CustomerDAO.update(customer);
            }else {
                CustomerDAO.insert(customer);
            }

            response.redirect("/customers");
            return response;
        } catch (Exception e){
            return renderAddUpdateForm(id,name,role,username,password,phoneNumber,ssn,e.getMessage());
        }

    }

    private static CustomerDTO validateAndBuildCustomer(String id, String name, String role, String username, String password,
                                                        String phoneNumber, String ssn){

        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;

        if (name == null || name.isEmpty()) {
            throw new RuntimeException("Name is required!");
        }

        if (role == null || role.isEmpty()) {
            throw new RuntimeException("Role is required!");
        }
        Role roleValue;
        try {
            roleValue = Role.valueOf(role);
        }catch (Exception e){
            throw new RuntimeException("Invalid role value: '" + role + "', must be one of: "+
                    Arrays.toString(Role.values()));
        }
        if (username == null || username.isEmpty()) {
            throw new RuntimeException("Username is required!");
        }
        if (password == null || password.isEmpty()) {
            throw new RuntimeException("Password is required!");
        }
        long phoneNumberValue = Long.parseLong(phoneNumber);
        long ssnValue = Long.parseLong(ssn);

        return new CustomerDTO(idValue, name, roleValue, username, password, phoneNumberValue, ssnValue);

    }
}
