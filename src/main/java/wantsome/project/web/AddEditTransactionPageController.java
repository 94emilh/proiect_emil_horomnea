package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionDTO;
import wantsome.project.db.dto.TransactionType;
import wantsome.project.db.service.TransactionDAO;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditTransactionPageController {

    public static String showAddForm(Request request, Response response) {
        return renderAddUpdateForm("", "", "", "","","", TransactionType.TRANSFER.name(), null);
    }

    public static String showUpdateForm(Request request, Response response){

        String id = request.params("id");
        Optional<TransactionDTO> optTransaction = TransactionDAO.get(Integer.parseInt(id));
        if (optTransaction.isEmpty()){
            throw new RuntimeException("Customer " + id + " not found!");
        }

        TransactionDTO transaction = optTransaction.get();
        return renderAddUpdateForm(
                String.valueOf(transaction.getId()),
                String.valueOf(transaction.getAccountId()),
                String.valueOf(transaction.getAmount()),
                String.valueOf(transaction.getDate()),
                transaction.getDetails(),
                String.valueOf(transaction.getDestinationAcc()),
                transaction.getType().name(),
                "");
    }

    private static String renderAddUpdateForm(String id, String accountId, String amount, String date, String details,
                                              String destinationAcc, String type, String errorMsg){
        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate",id!=null && !id.isEmpty());

        model.put("prevId",id);
        model.put("prevAccountId",accountId);
        model.put("prevAmount", amount);
        model.put("prevDate", date);
        model.put("prevDetails",details);
        model.put("prevDestinationAcc",destinationAcc);
        model.put("prevType",type);
        model.put("errorMsg",errorMsg);

        model.put("typeOptions",TransactionType.values());

        return render("add_edit_transaction.vm",model);
    }

    public static Object handleAddUpdatePostRequest(Request request,Response response){

        String id = request.queryParams("id");
        String accountId = request.queryParams("accountId");
        String amount = request.queryParams("amount");
        String date = request.queryParams("date");
        String details = request.queryParams("details");
        String destinationAcc = request.queryParams("destinationAcc");
        String type = request.queryParams("type");

        return tryPerformAddUpdateAction(response,id, accountId, amount, date, details, destinationAcc, type);
    }

    private static Object tryPerformAddUpdateAction (Response response, String id, String accountId, String amount, String date, String details, String destinationAcc, String type){
        try {
            TransactionDTO transaction = validateAndBuildTransaction(id, accountId, amount, date,details, destinationAcc, type);

            if(transaction.getId() > 0){
                TransactionDAO.update(transaction);
            }else {
                TransactionDAO.insert(transaction);
            }

            response.redirect("/transactions");
            return response;
        } catch (Exception e){
            return renderAddUpdateForm(id,accountId,amount,date,details,destinationAcc,type,e.getMessage());
        }

    }

    private static TransactionDTO validateAndBuildTransaction(String id, String accountId, String amount, String date, String details, String destinationAcc, String type){

        long idValue = id != null && !id.isEmpty() ? Long.parseLong(id) : -1;

        long accountIdValue = Long.parseLong(accountId);

        double amountvalue = Double.parseDouble(amount);


        Date dateValue = null;
        if(date!=null && !date.isEmpty()){
            try{
                dateValue = Date.valueOf(date);
            } catch(Exception e) {
                throw new RuntimeException("Invalid due date value: '" + date +
                        "', must be a date in format: yyyy-[m]m-[d]d");
            }
        }

        Long destinationAccValue = destinationAcc.isEmpty() ? null : Long.parseLong(destinationAcc);

        TransactionType typeValue;
        try{
            typeValue = TransactionType.valueOf(type);
        } catch (Exception e){
            throw new RuntimeException("Invalid transaction type: '" + type + "', must be one of: "+
                    Arrays.toString(TransactionType.values()));
        }


        return new TransactionDTO(idValue, accountIdValue, amountvalue, dateValue, details, destinationAccValue,typeValue);

    }
}
