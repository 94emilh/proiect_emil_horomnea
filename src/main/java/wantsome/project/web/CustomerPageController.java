package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.CustomerDTO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.service.CustomerDAO;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static wantsome.project.web.SparkUtil.render;

public class CustomerPageController {

    public enum Order {
        ROLE, NAME, BALANCE, DATE_OPENED, ID, TYPE, DATE, AMOUNT
    }

    public static String showCustomerPage(Request request, Response response) {
        List<CustomerDTO> listOfCustomers = CustomerDAO.getAll();

        //customers to display when filtering/sorting by role/name
        Role filter = getFilterFromQueryOrSes(request);
        Order order = getOrderFromQueryOrSes(request);
        List<CustomerDTO> customersToDisplay = getCustomersToDisplay(listOfCustomers, filter, order);

        //data for graphs
        long customersCount = listOfCustomers.stream()
                .filter(c -> c.getRole() == Role.CUSTOMER)
                .count();
        long adminsCount = listOfCustomers.size() - customersCount;

        Map<String, Object> model = new HashMap<>();

        model.put("customers", customersToDisplay);
        model.put("crtFilter", filter);
        model.put("crtOrder", order);
        model.put("customersCount", customersCount);
        model.put("adminsCount",adminsCount);

        return render("customer.vm", model);
    }

    private static Role getFilterFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "filter");
        return param == null ?
                Role.CUSTOMER : //default value (first time)
                (param.equals("ALL") ?
                        null :
                        Role.valueOf(param));
    }

    private static Order getOrderFromQueryOrSes(Request response) {
        String param = fromQueryOrSession(response, "order");
        return param == null ?
                Order.ROLE : //default
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name); //read it from current request (if sent)
        if (param != null) {
            request.session().attribute(name, param); //if found, save it to session for later
        } else {
            param = request.session().attribute(name); //if not found, try to read it from session
        }
        return param;
    }

    private static List<CustomerDTO> getCustomersToDisplay(List<CustomerDTO> allCustomers, Role roleFilter, Order order) {
        return allCustomers.stream()
                .filter(n -> roleFilter == null || n.getRole() == roleFilter)
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<CustomerDTO> getDisplayComparator(Order order) {
        if (order == Order.ROLE) {
            return Comparator.comparing(CustomerDTO::getRole);
        } else if (order == Order.ID){
            return Comparator.comparing(CustomerDTO::getId);
        }
        return Comparator.comparing(CustomerDTO::getName);

    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String id = request.params("id");
        CustomerDAO.delete(Long.parseLong(id));
        response.redirect("/customers");
        return response;
    }
}
