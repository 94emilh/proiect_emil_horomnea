package wantsome.project.web;

import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class HomePageController {
    public static String showHomepage(Request request, Response response) {

        Map<String,Object> model = new HashMap<>();


        return render("homepage.vm",model);
    }



}
