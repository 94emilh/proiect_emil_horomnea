package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.TransactionDAO;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static wantsome.project.web.SparkUtil.render;

public class TransactionPageController {

    public enum Order {
        ROLE, NAME, BALANCE, DATE_OPENED, ID, TYPE, DATE, AMOUNT
    }

    public static String showTransactionPage(Request request, Response response) {
        List<TransactionDTO> listOfTransactions = TransactionDAO.getAll();

        //transactions to display when filtering/sorting by type/date/amount/id
        TransactionType filter = getFilterFromQueryOrSes(request);
        Order order = getOrderFromQueryOrSes(request);
        List<TransactionDTO> transactionsToDisplay = getTransactionsToDisplay(listOfTransactions,filter,order);

        //data for graphs
        long transfersCount = listOfTransactions.stream()
                .filter(t -> t.getType() == TransactionType.TRANSFER)
                .count();
        long depositsCount = listOfTransactions.stream()
                .filter(d -> d.getType() == TransactionType.DEPOSIT)
                .count();
        long withdrawsCount = listOfTransactions.size() - transfersCount - depositsCount;
        long transactionVsAmountCount1 = computeTransactionAndAmount(listOfTransactions,1,100);
        long transactionVsAmountCount2 = computeTransactionAndAmount(listOfTransactions,100,500);
        long transactionVsAmountCount3 = computeTransactionAndAmount(listOfTransactions,500,1500);
        long transactionVsAmountCount4 = computeTransactionAndAmount(listOfTransactions,1500,Double.MAX_VALUE);


        Map<String, Object> model = new HashMap<>();

        model.put("transactions", transactionsToDisplay);
        model.put("crtFilter", filter);
        model.put("crtOrder", order);
        model.put("transfersCount",transfersCount);
        model.put("depositsCount",depositsCount);
        model.put("withdrawsCount",withdrawsCount);
        model.put("firstCategoryCount", transactionVsAmountCount1);
        model.put("secondCategoryCount", transactionVsAmountCount2);
        model.put("thirdCategoryCount", transactionVsAmountCount3);
        model.put("fourthCategoryCount", transactionVsAmountCount4);


        return render("transaction.vm", model);
    }

    public static long computeTransactionAndAmount(List<TransactionDTO> listOfTransactions, double minValue, double maxValue){
        return listOfTransactions.stream()
                .filter(s -> s.getAmount() >= minValue && s.getAmount() <maxValue)
                .count();
    }

    private static TransactionType getFilterFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "filter");
        return param == null ?
                TransactionType.TRANSFER : //default value (first time)
                (param.equals("ALL") ?
                        null :
                        TransactionType.valueOf(param));
    }

    private static Order getOrderFromQueryOrSes(Request response) {
        String param = fromQueryOrSession(response, "order");
        return param == null ?
                Order.ID : //default
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name); //read it from current request (if sent)
        if (param != null) {
            request.session().attribute(name, param); //if found, save it to session for later
        } else {
            param = request.session().attribute(name); //if not found, try to read it from session
        }
        return param;
    }

    private static List<TransactionDTO> getTransactionsToDisplay(List<TransactionDTO> allTransactions, TransactionType typeFilter, Order order) {
        return allTransactions.stream()
                .filter(n -> typeFilter == null || n.getType() == typeFilter)
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<TransactionDTO> getDisplayComparator(Order order) {
        if (order == Order.ID) {
            return Comparator.comparing(TransactionDTO::getId);
        } else if (order == Order.TYPE) {
            return Comparator.comparing(TransactionDTO::getType);
        } else if (order == Order.DATE) {
            Date defaultDate = new Date(0);
            return (i1, i2) -> {
                Date d1 = i1.getDate() != null ? i1.getDate() : defaultDate;
                Date d2 = i2.getDate() != null ? i2.getDate() : defaultDate;
                return d1.compareTo(d2);
            };
        }
        return Comparator.comparing(TransactionDTO::getAmount).reversed();
    }


    public static Object handleDeleteRequest(Request request, Response response) {
        String id = request.params("id");
        TransactionDAO.delete(Long.parseLong(id));
        response.redirect("/transactions");
        return response;
    }
}
