package wantsome.project;

import org.junit.*;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.AccountDAO;
import wantsome.project.db.service.CustomerDAO;
import wantsome.project.db.service.DbInitService;
import wantsome.project.db.service.TransactionDAO;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.*;

public class MainTest {

    private static final String TEST_DB_FILE = "project_test.db";
    private static final long currentTime = System.currentTimeMillis();
    private static final long DEFAULT_CUSTOMER_ID = 1;
    private static final long DEFAULT_ACCOUNT_ID = 10;
    private static final long DEFAULT_TRANSACTION_ID = 100;


    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DBManager.setDbFile(TEST_DB_FILE); //use a separate db for test, to avoid overwriting the real one
        DbInitService.createMissingTables();
    }

    private List<CustomerDTO> sampleCustomers;
    private List<AccountDTO> sampleAccounts;
    private List<TransactionDTO> sampleTransactions;


    @Before
    public void insertRowsBeforeTest() {
        //inserting sample customers
        CustomerDAO.insert(new CustomerDTO("Emil", Role.CUSTOMER, "94emilh", "parola", 7555124, 1940142113));
        CustomerDAO.insert(new CustomerDTO("Marta", Role.CUSTOMER, "95marta", "parola1", 7212315, 1925212317));
        CustomerDAO.insert(new CustomerDTO("Andrei", Role.ADMIN, "andreiadmin1", "parola2", 7771232, 190241252));

        sampleCustomers = CustomerDAO.getAll();
        //test expected order (sorted by name, different from insert order)
        assertEquals(
                "Andrei,Emil,Marta",
                sampleCustomers.stream()
                        .map(CustomerDTO::getName)
                        .sorted()
                        .collect(joining(",")));


        //inserting sample accounts
        AccountDAO.insert(new AccountDTO("Checking Acc 002", 5000, Date.valueOf("2020-01-02"), 2));
        AccountDAO.insert(new AccountDTO("Checking Acc 001", 1000, Date.valueOf("2020-01-01"), 1));

        sampleAccounts = AccountDAO.getAll();
        //test expected order (sorted by name, different from insert order)
        assertEquals(
                "Checking Acc 001,Checking Acc 002",
                sampleAccounts.stream()
                        .map(AccountDTO::getName)
                        .sorted()
                        .collect(Collectors.joining(",")));


        //inserting sample transactions
        TransactionDAO.insert(new TransactionDTO(1, 500, Date.valueOf("2020-01-05"), "N/A", 2L, TransactionType.TRANSFER));
        TransactionDAO.insert(new TransactionDTO(2, 1000, Date.valueOf("2020-01-01"), "N/A", null, TransactionType.DEPOSIT));
        TransactionDAO.insert(new TransactionDTO(2, 100, Date.valueOf("2020-01-10"), "N/A", null, TransactionType.WITHDRAW));


        sampleTransactions = TransactionDAO.getAll();
        //test expected order (sorted by name, different from insert order)
        assertEquals(
                "Checking Acc 001,Checking Acc 002",
                sampleTransactions.stream()
                        .map(TransactionDTO::getAmount)
                        .sorted()
                        .collect(Collectors.toList()));


    }

    @After
    public void deleteRowsAfterTest() {
        CustomerDAO.getAll().forEach(dto -> CustomerDAO.delete(dto.getId()));
        assertTrue(CustomerDAO.getAll().isEmpty());

        AccountDAO.getAll().forEach(dto -> AccountDAO.delete(dto.getId()));
        assertTrue(AccountDAO.getAll().isEmpty());

        TransactionDAO.getAll().forEach(dto -> TransactionDAO.delete(dto.getId()));
        assertTrue(TransactionDAO.getAll().isEmpty());
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleCustomersArePresentInDb();
        checkOnlyTheSampleAccountsArePresentInDb();
        checkOnlyTheSampleTransactionsArePresentInDb();
    }
//creating helping methods for getAll()
    private void checkOnlyTheSampleCustomersArePresentInDb() {
        List<CustomerDTO> dbCustomers = CustomerDAO.getAll();
        assertEquals(3, dbCustomers.size());
        assertEqualCustomersExceptId(sampleCustomers.get(0), dbCustomers.get(0));
        assertEqualCustomersExceptId(sampleCustomers.get(1), dbCustomers.get(1));
        assertEqualCustomersExceptId(sampleCustomers.get(2), dbCustomers.get(2));
    }

    private void assertEqualCustomersExceptId(CustomerDTO customerDTO1, CustomerDTO customerDTO2) {
        assertTrue("items should be equal (except ID): " + customerDTO1 + ", " + customerDTO2,
                customerDTO1.getName().equals(customerDTO2.getName()) &&
                        customerDTO1.getRole().equals(customerDTO2.getRole()) &&
                        customerDTO1.getUsername().equals(customerDTO2.getUsername()) &&
                        customerDTO1.getPassword().equals(customerDTO2.getPassword()) &&
                        customerDTO1.getPhoneNumber() == customerDTO2.getPhoneNumber() &&
                        customerDTO1.getSsn() == customerDTO2.getSsn());
    }

    private void checkOnlyTheSampleAccountsArePresentInDb() {
        List<AccountDTO> dbAccounts = AccountDAO.getAll();
        assertEquals(2, dbAccounts.size());
        assertEqualAccountsExceptId(sampleAccounts.get(0), dbAccounts.get(0));
        assertEqualAccountsExceptId(sampleAccounts.get(1), dbAccounts.get(1));
    }

    private void assertEqualAccountsExceptId(AccountDTO accountDTO1, AccountDTO accountDTO2) {
        assertTrue("items should be equal (except id): " + accountDTO1 + ", " + accountDTO2,
                accountDTO1.getName().equals(accountDTO2.getName()) &&
                        accountDTO1.getBalance().equals(accountDTO2.getBalance()) &&
                        accountDTO1.getDateOpened().equals(accountDTO2.getDateOpened()) &&
                        accountDTO1.getCustomerId() == accountDTO2.getCustomerId());
    }

    private void checkOnlyTheSampleTransactionsArePresentInDb() {
        List<TransactionDTO> dbTransactions = TransactionDAO.getAll();
        assertEquals(3, dbTransactions.size());
        assertEqualTransactionsExceptId(sampleTransactions.get(0), dbTransactions.get(0));
        assertEqualTransactionsExceptId(sampleTransactions.get(1), dbTransactions.get(1));
        assertEqualTransactionsExceptId(sampleTransactions.get(2), dbTransactions.get(2));
    }

    private void assertEqualTransactionsExceptId(TransactionDTO transactionDTO1, TransactionDTO transactionDTO2) {
        assertTrue("items should be equal (except id): " + transactionDTO1 + ", " + transactionDTO2,
                transactionDTO1.getAccountId() == transactionDTO2.getAccountId() &&
                        transactionDTO1.getAmount() == transactionDTO2.getAmount() &&
                        transactionDTO1.getDate().equals(transactionDTO2.getDate()) &&
                        transactionDTO1.getDetails().equals(transactionDTO2.getDetails()) &&
                        transactionDTO1.getDestinationAcc().equals(transactionDTO2.getDestinationAcc()) &&
                        transactionDTO1.getType().equals(transactionDTO2.getType()));
    }

    @Test
    public void get_forInvalidId(){
        assertFalse(CustomerDAO.get(-1).isPresent());
        assertFalse(AccountDAO.get(-1).isPresent());
        assertFalse(TransactionDAO.get(-1).isPresent());
    }

    @Test
    public void insert() {
        //testing the insert for Customers table
        assertEquals(3, CustomerDAO.getAll().size());

        CustomerDTO newCustomer = new CustomerDTO("Maria",Role.CUSTOMER, "maria20", "parolaaa", 75123414, 224215125);
        CustomerDAO.insert(newCustomer);

        assertEqualCustomersExceptId(newCustomer, CustomerDAO.getAll().get(CustomerDAO.getAll().size()-1));

        //testing the insert for Accounts table
        assertEquals(2, AccountDAO.getAll().size());

        AccountDTO newAccount = new AccountDTO("Checking Acc 003", 100000, Date.valueOf("2020-08-02"), 1);
        AccountDAO.insert(newAccount);

        assertEqualAccountsExceptId(newAccount,AccountDAO.getAll().get(AccountDAO.getAll().size()-1));

        //testing the insert for Transactions table
        assertEquals(3, TransactionDAO.getAll().size());

        TransactionDTO newTransaction = new TransactionDTO(1, 555, Date.valueOf("2020-04-10"), "N/A", null, TransactionType.WITHDRAW);
        TransactionDAO.insert(newTransaction);

        assertEqualTransactionsExceptId(newTransaction,TransactionDAO.getAll().get(TransactionDAO.getAll().size()-1));
    }

    @Test
    public void insert_nameAndUsernameAndSsnMustBeUnique() {
        assertEquals(3, CustomerDAO.getAll().size());
        CustomerDTO existingCustomer = sampleCustomers.get(1);

        CustomerDTO newCustomer = new CustomerDTO(-1, existingCustomer.getName(),Role.CUSTOMER,existingCustomer.getUsername(),"password",
                12345678,existingCustomer.getSsn());
        //insert should fail with an exception containing a specific message

        try{
            CustomerDAO.insert(newCustomer);
            Assert.fail("Insert customer with duplicate name/username/ssn should throw exception");
        } catch(RuntimeException rte){
            assertTrue(rte.getMessage().contains("A customer with same name/username/ssn already exists"));
        }

        //checking if there are still 3 customers in the DB and that newCustomer has not been added
        checkOnlyTheSampleCustomersArePresentInDb();
    }


}
